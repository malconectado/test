/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.test.test2;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;
/**
 *
 * @author Jesus Castillo
 */
public class Form extends JFrame implements KeyListener{
    private final static Logger log = Logger.getLogger(Form.class.getName());
    JTextField regexLabel = new JTextField("(('([^'].){0,}')|([j]{1})|(\\d+/\\d+)|(([Ii]nf)|(INF))|(nan)|([0-9]+(\\.[0-9]{0,})?([eE][+-]?[0-9]+)?)");
    JTextArea text = new JTextArea("'45544646'+1/2+inf*3.35*sin(j+(1+9))*3");
    JTextArea result = new JTextArea();
    Highlighter highlighter = text.getHighlighter();
    Form(String s){
        super(s);
        this.setSize(800, 300);
        this.setLocation(400, 100);
        Container c = this.getContentPane();
        c.setLayout(new BorderLayout(2, 2));
        
        c.add(regexLabel, BorderLayout.NORTH);
        JScrollPane sp = new JScrollPane(result);
        JSplitPane vsplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                                   text, sp);
        result.setEditable(false);
        vsplitPane.setOneTouchExpandable(true);
        vsplitPane.setDividerLocation(150);
        vsplitPane.setDividerSize(10);
        vsplitPane.setResizeWeight(1.0);
        c.add(vsplitPane,BorderLayout.CENTER);
        text.addKeyListener(this);
        regexLabel.addKeyListener(this); 
        text.setFont(new Font(Font.MONOSPACED,12,12));
        regexLabel.setFont(new Font(Font.MONOSPACED,12,12));
        result.setFont(new Font(Font.MONOSPACED,12,12));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    private void fireChange(){
        result.setText("");  
        int c = text.getCaretPosition();
        text.setText(text.getText());
        text.setCaretPosition(c);
        if (regexLabel.getText().isEmpty() || text.getText().isEmpty())
            return;
        
        Pattern p;
        try{
            p = Pattern.compile(regexLabel.getText());
        }catch (PatternSyntaxException e){
            result.append("Error in regex.");
            return;
        }
        
      HighlightPainter painter = 
             new DefaultHighlighter.DefaultHighlightPainter(Color.LIGHT_GRAY);
      
        
        
        Matcher m = p.matcher(text.getText());
        int i = 0;
        int lasti = 0;
        while (m.find()){
           
            result.append("Group " + i + ": " + m.group() + " -> (" + m.start()+"-"+m.end()+")" +"\n");
            try {
                highlighter.addHighlight(m.start(), m.start() + m.group().length(), painter );
                if (lasti!=0 && lasti==m.start())
                    highlighter.addHighlight(lasti, lasti, new DefaultHighlighter.DefaultHighlightPainter(Color.BLUE) );
                lasti=i;
            } catch (BadLocationException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
            i++;
        }        
        if (i==0){
            result.append("NO MATCH");
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        fireChange();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}//End Form
