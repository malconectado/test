package com.test.main;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * Test
 * @author jcastillo
 *
 */

/**

 * 4. Organizar el codigo (Opcional)
 * @author jcastillo
 *
 */
public class Main {
	Connection conn;
	Logger log = Logger.getLogger(Main.class.getCanonicalName());
	/**
	 * Init data base connection
	 */
    private void initDB(){
        if (conn!=null){
            return;
        }
        
        ServiceLoader<Driver> loader;
        loader = ServiceLoader.load(Driver.class);        
        Iterator<Driver> i = loader.iterator();
        Driver driver = null;
        if (i.hasNext()) {
        	driver = i.next();
        } 
        
        try { 
            DriverManager.registerDriver(driver);
            String dbinfo = AppProperties.get().getString("db.info");
            String dbuser = AppProperties.get().getString("db.user");
            String dbpassword = AppProperties.get().getString("db.password");
            
            conn = DriverManager.getConnection(dbinfo, dbuser, dbpassword);
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM information");
            PreparedStatement psu = conn.prepareStatement("UPDATE information SET rate = ?,lastmonitor=current_timestamp WHERE information_ID=?");
        	
            ps.execute();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
            	BigDecimal rate = rs.getBigDecimal("RATE");
            	int id =rs.getInt("information_id");
            	String description = rs.getString("description");
            	String type = rs.getString("type");
            	log.info("Info: " + rs.getString(1) + ", Type: " + type + ", Description: " + description);
            	
            	rate.add(BigDecimal.valueOf(0.01));
            	if (rs.getString("type").equals("NOTE")) {
            		psu.setBigDecimal(1, rate);
            		psu.setInt(2, id);          		
            		log.info("UPDATE: "+ id +" " + (psu.executeUpdate()>0?"SUCCESS":"FAIL"));            		
            	}
			}

            conn.commit();
            ps.close();
            psu.close();
            conn.close();
        } catch (SQLException ex) {
            log.warning(ex.getMessage());
            if (conn!=null) {
            	try {
					conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
        }
        
    }
	
	
	
	public static void main(String[] args) {
		System.setProperty("java.util.logging.SimpleFormatter.format",
	              "[%1$tT] %5$s %n");
		
		new Main().initDB();

	}

}
