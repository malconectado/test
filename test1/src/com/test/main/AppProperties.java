package com.test.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Properties;


public class AppProperties {

	private Properties props = new Properties();	
	private String PROPERTIES_DIR = "app.properties";	
	private static AppProperties instance;
	
	{//Load
		load(PROPERTIES_DIR);				
	}	
	
	
	public static AppProperties get() {
		if (instance == null) {
			instance = new AppProperties();
		}
		return instance;
	}
	
	/**
	 * 
	 */
	private AppProperties() {
		super();
	}

	/**
	 * Load from resource in root of jar
	 * @param name
	 * @return
	 */
	public boolean loadFromResources(String name) {
		InputStreamReader in;
		try {
			InputStream ins = ClassLoader.getSystemResourceAsStream(name);
			if (ins != null) {
				in = new InputStreamReader(ins);
				props.load(in);
				in.close();
			} else {
				return false;
			}
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Load from a resource in a specific package.<br>
	 * Need a class is that package.
	 * @param clasz class into package.
	 * @param name name of resource.
	 * @return true if file was successful loaded.
	 */
	public boolean load(Class<?> clasz,String name) {		
		InputStreamReader in;
		try {
			in = new InputStreamReader(clasz.getResource(name).openStream());
			props.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Load from a system path.
	 * @param pathname complete path of resource.
	 * @return true if file was successful loaded.
	 */
	public boolean load(String pathname) {
		File f = new File(pathname);
		if (!f.exists()) {
			return false;
		}
		try {
			FileReader r = new FileReader(f);
			props.load(r);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PROPERTIES_DIR = pathname;
		return true;
	}
	
	/**
	 * If you used load(String pathname) method 
	 * you can store change in original file
	 * @return true if file was successful loaded.
	 */
	public boolean store() {
		if (PROPERTIES_DIR==null) {
			return false;
		}
		OutputStream salida = null;
		try {
			salida = new FileOutputStream(PROPERTIES_DIR);
			props.store(salida, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (salida != null) {
				try {
					salida.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}
	/**
	 * Get double value or null if value does not exist
	 * @param name
	 * @return
     * @throws     NumberFormatException  if the string does not contain a
     *             parsable number.
	 */
	public Double getDouble(String name){
        String double0 = getString(name);
        if (double0==null){
            return null;
        }
        return Double.valueOf(double0);
    }
	/**
	 * Get integer value or null if value does not exist
	 * @param name
	 * @return
     * @throws     NumberFormatException  if the string does not contain a
     *             parsable number.
	 */
    public Integer getInteger(String name){
        String integer0 = getString(name);
        if (integer0==null){
            return null;
        }
        return Integer.valueOf(integer0);
    }
    
    /**
     * Return boolean value 
     * @param name
     * @return true if value is "Y", "y" or "true" else false 
     */
    public boolean getBoolean(String name){
        String flag = getString(name);
        if (flag==null){
            return false;
        }
        return "Y".equals(flag.toUpperCase()) 
        		|| "true".equals(flag);
    }
    
    /**
     * Get property as string
     * @param name
     * @return property value if exists
     */
    public String getString(String name){
    	if (name==null) {
    		return null;
    	}
        return props.getProperty(name);
    }
    
    public boolean setProperty(String key,String value) {
    	props.setProperty(key, value);
    	return true;
    }
    
    /**
     * Return a properties element
     * @return java.util.Properties
     */
    Properties getProperties() {
    	return props;
    }
    
    @Override
    public String toString() {
    	return props!=null? props.toString() : "{}";
    }	
}
